package scenarios;

import org.testng.annotations.Test;
import priority.Priority;

@Priority(1)
public class testClass2 {

    @Priority(8)
    @Test(description = "Test 3", groups = "Regression")
    public void Test3() {
        System.out.println(getClass().getName() + " -Test3");
    }

    @Priority(1)
    @Test(description = "Test 4", groups = "Regression")
    public void Test4() {
        System.out.println(getClass().getName() +" -Test4");
    }
}
