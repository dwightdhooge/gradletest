package scenarios;

import org.testng.annotations.Test;
import priority.Priority;

@Priority(1)
public class testClass1 {

    @Priority(4)
    @Test(description = "Test 1", groups = "Test")
    public void Test1() {
      System.out.println(getClass().getName() + " -Test1");
    }

    @Priority(5)
    @Test(description = "Test 2", groups = "Regression")
    public void Test2() {
        System.out.println(getClass().getName() + " -Test2");
    }
}
