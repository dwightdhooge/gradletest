package scenarios;

import org.testng.annotations.Test;
import priority.Priority;

@Priority(1)
public class testClass3 {

    @Priority(2)
    @Test(description = "Test 5", groups = "Regression")
    public void Test5() {
        System.out.println(getClass().getName() + " -Test5");
    }

    @Priority(10)
    @Test(description = "Test 6", groups = "Regression")
    public void Test6() {
        System.out.println(getClass().getName() + " -Test6");
    }
}
