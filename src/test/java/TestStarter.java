import org.testng.TestNG;
import org.testng.internal.PackageUtils;
import org.testng.xml.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that programmatically and dynamically performs the TestNG setup (instead of using a TestNG.XML file).
 * Performs the necessary test suite setup, looks for test methods in the project and launches them on all connected devices.
 */

public class TestStarter {
    private static final String TESTS_PACKAGE = "scenarios";

    public static void main(final String[] args) {

        runTestNg();
    }


    private static void runTestNg() {
        final XmlSuite suite = createTestSuite();
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);

//        for (TestDevice testDevice : devices) {
//            createTest(suite, testDevice);
//        }
        createTest(suite);

        final TestNG tng = new TestNG();
        tng.setXmlSuites(suites);
        tng.setUseDefaultListeners(false);
        System.out.println(suite);
        tng.run();
    }

    private static XmlSuite createTestSuite() {
        final XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");
        suite.setParallel(XmlSuite.ParallelMode.TESTS);
        suite.setThreadCount(4);
        suite.addListener("priority.PriorityInterceptor2");

        return suite;
    }

    private static void createTest(final XmlSuite suite) {

        XmlTest test = new XmlTest(suite);
        test.setName("TmpTest");

        Map<String, String> parameters = new HashMap<>();
//        parameters.put(Constants.DEVICE_ID, testDevice.getDeviceId());
//        parameters.put(Constants.DEVICE_NAME, getDeviceName(testDevice));
//        parameters.put(Constants.PLATFORM_VERSION, getOsVersion(testDevice));
//        parameters.put(Constants.PLATFORM_NAME, testDevice.getPlatform().getName());

        test.setParameters(parameters);
        test.setPreserveOrder(true);

        addGroups(test);

        addClasses(test);

    }

    private static void addClasses(XmlTest test) {

        final List<XmlClass> myClasses = new ArrayList<XmlClass>();
//        String testClass = System.getProperty("testClass1");
//        System.out.println(testClass);
//        if (testClass.equals("all")) {
//            final List<XmlPackage> packages = new ArrayList<>();
//            packages.add(new XmlPackage(TESTS_PACKAGE));
//            test.setPackages(packages);
//            System.out.println("Package is set");

            try {
                String[] testClasses = PackageUtils.findClassesInPackage(TESTS_PACKAGE, new ArrayList<String>(), new ArrayList<>());
                for (String eachClass : testClasses) {
                    myClasses.add(new XmlClass(eachClass));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            test.setXmlClasses(myClasses);


//        } else {
//            int i = 1;
//            while (testClass != null && !testClass.isEmpty() && !testClass.equals("false") && !testClass.equals("0")) {
//
//                System.out.println("testClass found:" + testClass);
//                myClasses.add(new XmlClass(TESTS_PACKAGE + "." + testClass));
//                System.out.println("testClass added:" + testClass);
//
//                i++;
//                testClass = System.getProperty("testClass" + i);
//                System.out.println("Next testClass " + i + testClass);
//
//            }
////            test.setXmlClasses(myClasses);
//            System.out.println("set" + myClasses);
//            System.out.println("TestClasses set");
//
//
//        }            test.setXmlClasses(myClasses);
//
//
//    }

    //    private static void addClasses(XmlTest test) {
//        final String testClass = System.getProperty("testClass");
//        if (testClass.equals("all")) {
//            final List<XmlPackage> packages = new ArrayList<>();
//            packages.add(new XmlPackage(TESTS_PACKAGE));
//            test.setPackages(packages);
//        } else {
//            final List<XmlClass> myClasses = new ArrayList<XmlClass>();
//            myClasses.add(new XmlClass(TESTS_PACKAGE + "." + testClass));
//            test.setXmlClasses(myClasses);
//        }
    }

    private static void addGroups(XmlTest test) {
        final String testGroup = System.getProperty("testGroup");
        if (!testGroup.equals("all")) {
            XmlGroups xmlGroups = createGroupIncluding(testGroup);
            test.setGroups(xmlGroups);
        }
    }

    private static XmlGroups createGroupIncluding(String testGroup) {
        final XmlGroups xmlGroups = new XmlGroups();
        final XmlRun xmlRun = new XmlRun();
        xmlRun.onInclude(testGroup);
        xmlGroups.setRun(xmlRun);
        return xmlGroups;
    }


}
