package priority;

import org.testng.IMethodInstance;
import org.testng.IMethodInterceptor;
import org.testng.ITestContext;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class PriorityInterceptor2 implements IMethodInterceptor {

    @Override
    public List<IMethodInstance> intercept(List<IMethodInstance> methods,
                                           ITestContext context) {

        Comparator<IMethodInstance> comparator = new Comparator<IMethodInstance>() {

            private int getPriority(IMethodInstance mi) {
                int result = 0;
                int methodPrio = 0;
                int classPrio = 0;
                Method method = mi.getMethod().getConstructorOrMethod().getMethod();
                Priority methodPriority = method.getAnnotation(Priority.class);
                Class<?> cls = method.getDeclaringClass();
                Priority classPriority = cls.getAnnotation(Priority.class);

                methodPrio = methodPriority == null ? 0 : methodPriority.value();
                classPrio = classPriority == null ? 0 : classPriority.value();

                result = Integer.valueOf(String.valueOf(classPrio) + String.valueOf(methodPrio));


//                if (methodPriority != null) {
//                    methodPrio = methodPriority.value();
//                    classPrio = classPriority == null ? 0 : classPriority.value();
//
//                    result = Integer.valueOf(String.valueOf(classPrio) + String.valueOf(methodPrio));
//
//                } else {
//
//                    if (classPriority != null) {
//                        classPrio = classPriority.value();
//                        result = Integer.valueOf(String.valueOf(classPrio) + "0");
//                    } else {
//                        result = 0;
//                    }
//                }
                System.out.println("Priority of " + method.getName() + " in " + method.getDeclaringClass().getName() + " is " + result);
                return result;
            }

            @Override
            public int compare(IMethodInstance m1, IMethodInstance m2) {
                return getPriority(m1) - getPriority(m2);
            }
        };
        Collections.sort(methods, comparator);
        System.out.println(methods);
        return methods;
        /*IMethodInstance[] array = methods.toArray(new IMethodInstance[methods.size()]);
        Arrays.sort(array, comparator);
        System.out.println("Ordered list =" + Arrays.asList(array));

        return Arrays.asList(array);*/


    }

}


